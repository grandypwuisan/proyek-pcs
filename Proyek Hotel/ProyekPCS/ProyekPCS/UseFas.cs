﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class UseFas : Form
    {
        public UseFas()
        {
            InitializeComponent();
        }
        OracleConnection oc;

        private void UseFas_Load(object sender, EventArgs e)
        {
            this.Size = this.Parent.Size;

            this.Location = new Point(0, 0);
            try
            {
                oc = new OracleConnection
                ("user id=system;" +
                 "password=maximillian99;" +
                  "data source=orcl");
                oc.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Gagal karena" + ex.Message);
            }

            refresh();
            comb1();
            //comb2();
        }

        public void refresh()
        {
            string q = "select * from usefas";
            OracleDataAdapter oda = new OracleDataAdapter(q, oc);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            dataGridView1.DataSource = dt;

            string q1 = "select * from fasilitas";
            OracleDataAdapter oda1 = new OracleDataAdapter(q1, oc);
            DataTable dt1 = new DataTable();
            oda1.Fill(dt1);
            dataGridView2.DataSource = dt1;
        }

        public void comb1()
        {

            string q = "select * from FASILITAS";
            OracleDataAdapter oda = new OracleDataAdapter(q, oc);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            comboBox1.ValueMember = "ID_FAS";
            comboBox1.DisplayMember = "NAMA_FAS";
            comboBox1.DataSource = dt;
            comboBox1.SelectedIndex = 0;

        }

        public void comb2()
        {
            string q = "Select NO_KAMAR FROM TRANSAKSI WHERE STATUS_TRANS = 1";
            OracleDataAdapter oda = new OracleDataAdapter(q,oc);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            comboBox2.ValueMember = "NO_KAMAR";
            comboBox2.DisplayMember = "NO_KAMAR";
            comboBox2.DataSource = dt;
            comboBox2.SelectedIndex = 0;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dataGridView2.Rows.Count-1; i++)
                {
                    if (dataGridView2[0,i].Value.ToString().Equals(comboBox1.SelectedValue.ToString()))
                    {
                        if (dataGridView2[3, i].Value.ToString().Equals("Not Available"))
                        {
                            MessageBox.Show("Fasilitas belum dapat dipakai");
                        }
                        else
                        {
                            string q = "insert into usefas values ('" + comboBox1.SelectedValue.ToString() + "','ID001'," + numericUpDown1.Value + ")";
                            OracleCommand cmd = new OracleCommand(q, oc);
                            cmd.ExecuteNonQuery();
                            refresh();
                        }
                        
                    }
                }
                
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
