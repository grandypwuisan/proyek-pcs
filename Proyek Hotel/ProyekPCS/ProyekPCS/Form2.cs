﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;


namespace ProyekPCS
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        OracleConnection oc;



        private void Form2_Load(object sender, EventArgs e)
        {
            this.Size = this.Parent.Size;
            
            this.Location = new Point(0, 0);
            try
            {
                oc = new OracleConnection
                ("user id=system;" +
                 "password=maximillian99;" +
                  "data source=orcl");
                oc.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Gagal karena" + ex.Message);
            }

            refresh();

        }

        public void refresh()
        {
            string q = "select * from fasilitas";
            OracleDataAdapter oda = new OracleDataAdapter(q,oc);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            dataGridView1.DataSource = dt;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            string q = "INSERT INTO FASILITAS VALUES('','" + textBox2.Text + "'," + numericUpDown1.Value + ", 'Available')";
            try
            {
                OracleCommand cmd = new OracleCommand(q,oc);
                cmd.ExecuteNonQuery();
                refresh();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowind = e.RowIndex;
            textBox3.Text = dataGridView1[0, rowind].Value.ToString();
            textBox1.Text = dataGridView1[1, rowind].Value.ToString();
            numericUpDown2.Value = Int32.Parse( dataGridView1[2, rowind].Value.ToString());
            if (dataGridView1[3,rowind].Value.ToString().Equals("Available"))
            {
                comboBox1.SelectedIndex = 0;
            }
            else
            {
                comboBox1.SelectedIndex = 1;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string q = "UPDATE FASILITAS SET NAMA_FAS ='" + textBox1.Text +
                "' WHERE ID_FAS = '" + textBox3.Text + "'";
                OracleCommand cmd = new OracleCommand(q, oc);
                cmd.ExecuteNonQuery();
                q = "UPDATE FASILITAS SET HARGA_FAS =" + Int32.Parse( numericUpDown2.Text) +
                    " WHERE ID_FAS = '" + textBox3.Text + "'";
                OracleCommand cmd1 = new OracleCommand(q, oc);
                cmd1.ExecuteNonQuery();
                q = "UPDATE FASILITAS SET STATUS_FAS ='" + comboBox1.Text +
                    "' WHERE ID_FAS = '" + textBox3.Text + "'";
                OracleCommand cmd2 = new OracleCommand(q, oc);
                cmd2.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("gagal karena" + ex.Message);
            }

            refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
