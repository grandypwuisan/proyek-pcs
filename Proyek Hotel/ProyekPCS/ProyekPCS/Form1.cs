﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyekPCS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            FormLogin p = new FormLogin();
            f2.MdiParent = this.ParentForm;
            f2.Show();
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            UseFas f2 = new UseFas();
            FormLogin p = new FormLogin();
            f2.MdiParent = this.ParentForm;
            f2.Show();
            this.Close();
        }
    }
}
