﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class MasterJenisKamar : Form
    {
        public MasterJenisKamar()
        {
            InitializeComponent();
        }
        OracleConnection OC;
        OracleDataAdapter oda;
        OracleCommand cmd;
        OracleCommandBuilder ocomb;
        DataTable dt;
        private void label1_Click(object sender, EventArgs e)
        {
            
        }
        FormUtama parent;
        private void MasterJenisKamar_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            textBox1.Enabled = false;
            try
            {
                OC = new OracleConnection("user id=system;password=system;data source=orcl");
                OC.Open();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Isi_Grid();
        }
        void Isi_Grid()
        {
            try{
                oda = new OracleDataAdapter("select * from JENIS_KAMAR", OC);
                dt = new DataTable();
                ocomb = new OracleCommandBuilder(oda);
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        string query = "";
        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox2.Text.Length > 2)
            {
                string harga = numericUpDown1.Value.ToString();
                query = "insert into JENIS_KAMAR values('" + textBox1.Text + "','" + textBox2.Text + "'," + harga + ",'" + textBox4.Text + "')";
                try
                {
                    cmd = new OracleCommand(query, OC);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Jenis Kamar " + textBox2.Text + " Telah Ditambahkan\nKode : " + textBox1.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                Isi_Grid();
                Refresh_Kotak1();
            }
            else
            {
                MessageBox.Show("Nama Kamar harus lebih dari dua huruf");
            }
            
        }

        void Refresh_Kotak1()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            numericUpDown1.Value = 0;
            textBox4.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            query = "select AUTOGENIDJK('" + textBox2.Text + "') from dual";
            cmd = new OracleCommand(query, OC);
            textBox1.Text = cmd.ExecuteScalar().ToString();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox7.Text =  dataGridView1[0, e.RowIndex].Value.ToString();
            textBox6.Text = dataGridView1[1, e.RowIndex].Value.ToString();
            numericUpDown2.Value = Convert.ToInt32(dataGridView1[2, e.RowIndex].Value.ToString());
            textBox8.Text = dataGridView1[3, e.RowIndex].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string temp = numericUpDown2.Value.ToString();
            try
            {
                query = "update JENIS_KAMAR set nama_jenis = '" + textBox6.Text + "', harga_kamar = " + temp + ", " +
                "DESKRIPSI_KAMAR = '" + textBox8.Text + "' where kode_jenis = '" + textBox7.Text + "'";
                cmd = new OracleCommand(query, OC);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Jenis Kamar " + textBox6.Text + " berhasil diupdate");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            refresh_kotak2();
        }
        void refresh_kotak2()
        {
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            numericUpDown2.Value = 0;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }
    }
}
