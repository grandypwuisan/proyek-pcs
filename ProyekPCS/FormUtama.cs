﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyekPCS
{
    public partial class FormUtama : Form
    {
        public string userLogin, IduserLogin;
        public FormUtama()
        {
            InitializeComponent();
            userLogin = "Admin";
        }

        private void FormUtama_Load(object sender, EventArgs e)
        {
            showFormLogin();
        }
        public void showMenuUtama()
        {
            MenuUtama menu = new MenuUtama();
            menu.MdiParent = this;
            menu.WindowState = FormWindowState.Maximized;
            menu.FormBorderStyle = FormBorderStyle.None;
            menu.Show();
        }
        public void setUserLogin(string s)
        {
            userLogin = s;
        }
        public string getUserLogin()
        {
            return userLogin;
        }
        public void setIdUserLogin(string s)
        {
            IduserLogin = s;
        }
        public string getIdUserLogin()
        {
            return IduserLogin;
        }
        public void showMasterJenisKamar()
        {
            MasterJenisKamar masterjk = new MasterJenisKamar();
            masterjk.MdiParent = this;
            masterjk.WindowState = FormWindowState.Maximized;
            masterjk.Show();
        }
        public void showFormLogin()
        {
            FormLogin formLogin = new FormLogin();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }
        public void showMasterKamar()
        {
            MasterKamar masterKamar = new MasterKamar();
            masterKamar.MdiParent = this;
            masterKamar.WindowState = FormWindowState.Maximized;
            masterKamar.Show();
        }
        public void showMasterPegawai()
        {
            MasterPegawai masterPegawai = new MasterPegawai();
            masterPegawai.MdiParent = this;
            masterPegawai.WindowState = FormWindowState.Maximized;
            masterPegawai.Show();
        }
        public void showMasterCustomer()
        {
           MasterCustomer masterCustomer = new MasterCustomer();
            masterCustomer.MdiParent = this;
            masterCustomer.WindowState = FormWindowState.Maximized;
            masterCustomer.Show();
        }
        public void showMasterMenu()
        {
            Menu masterMenu = new Menu();
            masterMenu.MdiParent = this;
            masterMenu.WindowState = FormWindowState.Maximized;
            masterMenu.Show();
        }
        public void showMasterFas()
        {
            Form2 masterFas = new Form2();
            masterFas.MdiParent = this;
            masterFas.WindowState = FormWindowState.Maximized;
            masterFas.Show();
        }
        public void showTransaksi()
        {
            FormTransaksi trans = new FormTransaksi();
            trans.MdiParent = this;
            trans.WindowState = FormWindowState.Maximized;
            trans.Show();
        }
        public void showTransaksiMakanan()
        {
            PemesanMenu trans1 = new PemesanMenu();
            trans1.MdiParent = this;
            trans1.WindowState = FormWindowState.Maximized;
            trans1.Show();
        }
        public void showTransaksiFasilitas()
        {
            UseFas trans2 = new UseFas();
            trans2.MdiParent = this;
            trans2.WindowState = FormWindowState.Maximized;
            trans2.Show();
        }
        public void showCheckout()
        {
            checkout cout = new checkout();
            cout.MdiParent = this;
            cout.WindowState = FormWindowState.Maximized;
            cout.Show();
        }

    }
}
