﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
namespace ProyekPCS
{
    public partial class PemesanMenu : Form
    {
        OracleConnection conn = new OracleConnection("data source=orcl; user id=system;password=system;");
        public PemesanMenu()
        {
            InitializeComponent();
        }

        FormUtama parent;
        private void PemesanMenu_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            conn.Open();
            isimenu();
            insert(comboBox1);
            textBox1.Enabled = false;
            tampilData();
        }
        public void tampilData()
        {
            OracleCommand cmd = new OracleCommand("select * from pemesanan_menu", conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
        }
        public void insert(ComboBox combo)
        {
            try
            {
                
                OracleCommand cmd = new OracleCommand("select  no_kamar from transaksi where status_trans=1", conn);
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {





                        combo.Items.Add(dataGridView1.Rows[i].Cells[0].Value.ToString());


                        


            }
        }
        public void isimenu()
        {
            OracleCommand cmd = new OracleCommand("select NAMA_MENU,ID_MENU from menu", conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
           
            comboBox2.DataSource = ds.Tables[0];
            comboBox2.DisplayMember = "NAMA_MENU";
            comboBox2.ValueMember = "ID_MENU";



        }
        public string cariid()
        {

            string isi;

            OracleCommand cmd = new OracleCommand("select id_transaksi from transaksi where no_kamar='" + comboBox1.Text + "' and status_trans=1", conn); OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            isi = (cmd.ExecuteScalar().ToString());


            return isi;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                try
                {

                    string id = "";
                    id = cariid();
                    string temp = comboBox2.SelectedValue + "";

                    int jumlah = Int32.Parse(numericUpDown1.Value.ToString());
                    string sub = textBox1.Text;
                    string query = "insert into PEMESANAN_MENU values('" + id + "','" + temp + "'," + jumlah + "," + sub + ")";
                    OracleCommand cmd = new OracleCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Test");

                }
                catch (Exception ex)
                {
                    string id = "";
                    id = cariid();
                    string temp = comboBox2.SelectedValue + "";


                    int jumlah = Int32.Parse(numericUpDown1.Value.ToString());
                    string sub = textBox1.Text;

                    ; string query = "update PEMESANAN_MENU set jumlah=jumlah+" + jumlah + ",subtotal=subtotal+" + sub + "where id_transaksi='" + id + "' and id_menu='" + temp + "'";
                    OracleCommand cmd = new OracleCommand(query, conn);
                    cmd.ExecuteNonQuery();

                }



            }
            tampilData();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand("SELECT HARGA_MENU FROM MENU WHERE ID_MENU='" + comboBox2.SelectedValue.ToString() + "'", conn);
            int isi = Int32.Parse(cmd.ExecuteScalar().ToString());
            textBox1.Text = (Int32.Parse(numericUpDown1.Value.ToString()) * isi) + "";
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            numericUpDown1.Value = 0;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
