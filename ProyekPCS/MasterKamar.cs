﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class MasterKamar : Form
    {
        OracleConnection OC;
        OracleDataAdapter oda;
        OracleCommand cmd;
        OracleCommandBuilder ocomb;
        DataTable dt;
        public MasterKamar()
        {
            InitializeComponent();
        }
        bool penanda = true;
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() == "")
            {
                query = "select * from TAMPILAN_FORM_KAMAR where JENIS_KAMAR LIKE '%" + textBox4.Text + "%'";
                Isi_Grid(query);
            }
            else
            {
                query = "select * from TAMPILAN_FORM_KAMAR where SUBSTR(NOMOR_KAMAR,0,1) = '" + comboBox2.SelectedValue.ToString() + "' AND " +
                "JENIS_KAMAR LIKE '%" + textBox4.Text + "%'";
                Isi_Grid(query);
            }
        }

        FormUtama parent;
        private void MasterKamar_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            textBox3.Enabled = false;
            try
            {
                OC = new OracleConnection("user id=system;password=system;data source=orcl");
                OC.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Isi_combox();
            Isi_Grid("select * from TAMPILAN_FORM_KAMAR");
            isiCombox2();
            isi_grid2();
            penanda = false;
        }
        void Isi_combox()
        {
            oda = new OracleDataAdapter("select * from jenis_kamar", OC);
            dt = new DataTable();
           oda.Fill(dt);
            comboBox1.DisplayMember = "NAMA_JENIS";
            comboBox1.ValueMember = "KODE_JENIS";
            comboBox1.DataSource = dt;
            comboBox3.DisplayMember = "NAMA_JENIS";
            comboBox3.ValueMember = "KODE_JENIS";
            comboBox3.DataSource = dt;
        }
        void Isi_Grid(string query)
        {
            try
            {
                oda = new OracleDataAdapter(query, OC);
                dt = new DataTable();
                ocomb = new OracleCommandBuilder(oda);
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        void isi_grid2()
        {
            oda = new OracleDataAdapter("select * from jenis_kamar", OC);
            dt = new DataTable();
            ocomb = new OracleCommandBuilder(oda);
            oda.Fill(dt);
            dataGridView2.DataSource = dt;
        }
        string query = "";
        private void button1_Click(object sender, EventArgs e)
        {
            int angka1 = (int)numericUpDown1.Value;
            int angka2 = (int)numericUpDown2.Value;
            if(angka1 < 101 || angka2 < 101)
            {
                MessageBox.Show("Harus Menigkuti Format Angka ratusan");
            }
            else if(angka1/100 == angka2/100 && angka1 < angka2)
            {
                for (int i = angka1; i <= angka2; i++)
                {
                    try
                    {
                        query = "insert into KAMAR values('" + i.ToString() + "','" + comboBox1.SelectedValue.ToString() + "',1)";
                        cmd = new OracleCommand(query, OC);
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                MessageBox.Show("Berhasil membuat kamar " + angka1.ToString() + " sampai " + angka2.ToString());
            }
            else if(angka2 == 0)
            {
                try
                {
                    query = "insert into KAMAR values('" + angka1.ToString() + "','" + comboBox1.SelectedValue.ToString() + "',1)";
                    cmd = new OracleCommand(query, OC);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                MessageBox.Show("Berhasil membuat kamar " + angka1.ToString());
            }
            else
            {
                MessageBox.Show("Lantai kamar harus sama dan nomor kamar harus berurut dari kecil");
            }
            Isi_Grid("select * from TAMPILAN_FORM_KAMAR");
            numericUpDown1.Value = 0;
            numericUpDown2.Value = 0;
            isiCombox2();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(textBox3.Text != "")
            {
                cmd = new OracleCommand("select kode_jenis from jenis_kamar where nama_jenis = '" + comboBox3.Text + "'", OC);
            string kode_kamar = cmd.ExecuteScalar().ToString();
            try
            {
                query = "update kamar set kode_jenis = '" + kode_kamar + "' where no_kamar = '" + textBox3.Text + "'";
                cmd = new OracleCommand(query, OC);
                cmd.ExecuteNonQuery();
                
            }
                catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Isi_Grid("select * from TAMPILAN_FORM_KAMAR");
        }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox3.Text = dataGridView1[0, e.RowIndex].Value.ToString();
            comboBox3.Text = dataGridView1[1, e.RowIndex].Value.ToString();
        }
        void isiCombox2()
        {
            oda = new OracleDataAdapter("select * from LANTAI_FORM_KAMAR", OC);
            dt = new DataTable();
            oda.Fill(dt);
            comboBox2.DisplayMember = "lantai";
            comboBox2.ValueMember = "kode";
            comboBox2.DataSource = dt;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!penanda)
            {
                if (comboBox2.SelectedValue.ToString() == "")
                {
                    query = "select * from TAMPILAN_FORM_KAMAR where JENIS_KAMAR LIKE '%" + textBox4.Text + "%'";
                    Isi_Grid(query);
                }
                else
                {
                    query = "select * from TAMPILAN_FORM_KAMAR where SUBSTR(NOMOR_KAMAR,0,1) = '" + comboBox2.SelectedValue.ToString() + "' AND " +
                    "JENIS_KAMAR LIKE '%" + textBox4.Text + "%'";
                    Isi_Grid(query);
                }
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }
    }
}
