﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyekPCS
{
    public partial class MenuUtama : Form
    {
        FormUtama parent;
        public MenuUtama()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            parent.showMasterCustomer();
            this.Close();
        }

        private void MenuUtama_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            label3.Text = "Halo "+parent.getUserLogin();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            parent.showMasterPegawai();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            parent.showMasterKamar();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            parent.showMasterJenisKamar();
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            parent.showTransaksi();
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            parent.showMasterFas();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            parent.showMasterMenu();
            this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            parent.showTransaksiMakanan();
            this.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            parent.showTransaksiFasilitas();
            this.Close();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            parent.showCheckout();
            this.Close();
        }
    }
}
