﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class FormLogin : Form
    {
        public OracleConnection conn;
        public FormLogin()
        {
            InitializeComponent();
            try
            {
                conn = new OracleConnection("user id=system;password=system;data source=orcl");
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user = textBox1.Text;
            string pass = textBox2.Text;
            if(user == pass)
            {
                if (user == "admin")
                {
                    MessageBox.Show("Selamat Datang Admin");
                    this.Close();
                    parent.showMenuUtama();
                }
                else
                {
                    try
                    {
                        string query = "select * from pegawai where id_pegawai = '" + user + "'";
                        OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                        DataTable dt = new DataTable();
                        oda.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            MessageBox.Show("Selamat Datang " + dt.Rows[0][1].ToString());
                            this.Close();
                            parent.setUserLogin(dt.Rows[0][1].ToString());
                            parent.setUserLogin(user);
                            parent.showMenuUtama();
                        }
                        else
                        {
                            MessageBox.Show("username atau password salah");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }
        FormUtama parent; 
        private void FormLogin_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
        }
    }
}
