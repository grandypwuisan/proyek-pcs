﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class MasterCustomer : Form
    {
        public OracleConnection conn;
        public MasterCustomer()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        void refreshData()
        {
            string query = "select * from customer";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        FormUtama parent;
        private void MasterCustomer_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            try
            {
                conn = new OracleConnection("user id=system;password=system;data source = orcl");
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            refreshData();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int row = e.RowIndex;
            textBox9.Text = dataGridView1[0, row].Value.ToString();
            textBox6.Text = dataGridView1[1, row].Value.ToString();
            textBox8.Text = dataGridView1[2, row].Value.ToString();
            textBox7.Text = dataGridView1[3, row].Value.ToString();
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            string nama = textBox2.Text;
            if (nama.Length >= 2)
            {
                OracleCommand cmd = new OracleCommand("AUTO_GEN_ID_CUST_HOTEL", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter hasil = new OracleParameter();
                hasil.Direction = ParameterDirection.ReturnValue;
                hasil.OracleDbType = OracleDbType.Varchar2;
                hasil.Size = 5;
                cmd.Parameters.Add(hasil);

                OracleParameter parInput = new OracleParameter();
                parInput.Value = textBox2.Text;
                parInput.Direction = ParameterDirection.Input;
                parInput.OracleDbType = OracleDbType.Varchar2;
                parInput.Size = 50;
                cmd.Parameters.Add(parInput);
                cmd.ExecuteNonQuery();
                textBox1.Text = hasil.Value.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox4.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                if (textBox2.Text.Length < 2)
                {
                    MessageBox.Show("Nama kurang dari 2 huruf");
                }
                else
                {
                    if (textBox3.Text.Length >= 10 && textBox3.Text.Length <= 12)
                    {
                        //insert
                        string id = textBox1.Text;
                        string nama = textBox2.Text;
                        string nohp = textBox3.Text;
                        string alamat = textBox4.Text;
                        try
                        {
                            string query = "insert into customer values('" + id + "','" + nama + "', '"+alamat+"', '"+nohp+"')";
                            OracleCommand cmd = new OracleCommand(query, conn);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Berhasil Insert");
                            refreshData();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Nomor HP harus antara 10-12 digit");
                    }
                }
            }
            else
            {
                MessageBox.Show("Pastikan semua field terisi");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string id = textBox9.Text;
                string nama = textBox6.Text;
                string nohp = textBox7.Text;
                string alamat = textBox8.Text;
                string query = "update customer set nama_cust = '" + nama + "', notelp_cust = '" + nohp + "',alamat_cust = '"+alamat+"' where id_cust = '" + id + "'";
                OracleCommand cmd = new OracleCommand(query, conn);
                cmd.ExecuteNonQuery();
                refreshData();
                MessageBox.Show("berhasil update");
                textBox9.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string id = textBox9.Text;
                string query = "delete from customer where id_cust = '" + id + "'";
                OracleCommand cmd = new OracleCommand(query, conn);
                cmd.ExecuteNonQuery();
                refreshData();
                MessageBox.Show("berhasil delete");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string nama = textBox5.Text;
            string query = "select * from customer where upper(nama_cust) like upper('%" + nama + "%')";
            OracleDataAdapter oda = new OracleDataAdapter(query, conn);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            refreshData();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }
    }
}
