﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
namespace ProyekPCS
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }
        OracleConnection conn = new OracleConnection("user id=system;password=system;data source=orcl");
        public void tampildata()
        {
            conn.Open();
            string query = "select * from menu";
            OracleDataAdapter oda = new OracleDataAdapter(query, conn);
            DataTable dt = new DataTable();

            oda.Fill(dt);
            dataGridView1.DataSource = dt;
            conn.Close();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        FormUtama parent;
        private void Menu_Load(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            tampildata();
            parent = (FormUtama)this.MdiParent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conn.Open();
            string kode = textBox2.Text.Substring(0, 2);
            string nama = textBox2.Text;
            string Harga = textBox3.Text;

            string query1 = "select count(*) from menu where substr(id_menu,1,2)='" + kode + "'";

            OracleCommand cmd = new OracleCommand(query1, conn);
            int temp = Int32.Parse(cmd.ExecuteScalar().ToString()) + 1;
            if (temp < 10)
            {
                kode = kode + "00" + temp;
            }
            else if (temp < 100)
            {
                kode = kode + "0" + temp;
            }
            else if (temp < 100)
            {
                kode = kode + "" + temp;
            }
            kode = kode.ToUpper();
            textBox1.Text = kode;
            string query = "insert into menu values('" + kode + "','" + nama + "'," + Harga + ")";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            conn.Close();
            tampildata();
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1[0, e.RowIndex].Value.ToString();
            textBox2.Text = dataGridView1[1, e.RowIndex].Value.ToString();
            textBox3.Text = dataGridView1[2, e.RowIndex].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conn.Open();
            string kode = textBox1.Text;
            string nama = textBox2.Text;
            string harga = textBox3.Text;
            string query = "update menu set nama_menu='" + nama + "',harga_menu=" + harga + " where id_menu='" + kode + "'";

            OracleCommand cmd = new OracleCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            tampildata();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Text != "")
            {
                if (comboBox1.Text == "Kode Menu")
                {
                    conn.Open();
                    string query = "select * from menu where upper(id_menu) like '%" + textBox4.Text.ToUpper() + "%'";
                    OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                    DataTable dt = new DataTable();
                    oda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    conn.Close();
                }
                else if (comboBox1.Text == "Nama Menu")
                {
                    conn.Open();
                    string query = "select * from menu where upper(nama_menu) like '%" + textBox4.Text.ToUpper() + "%'";
                    OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                    DataTable dt = new DataTable();
                    oda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    conn.Close();
                }
                else if (comboBox1.Text == "Harga Lebih Besar Dari")
                {
                    try
                    {
                        conn.Open();
                        string temp = "0";
                        if (textBox4.Text != "")
                        {
                            temp = textBox4.Text;
                        }
                        string query = "select * from menu where harga_menu>" + temp + "";
                        OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                        DataTable dt = new DataTable();
                        oda.Fill(dt);
                        dataGridView1.DataSource = dt;
                        conn.Close();
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("Tidak Boleh ada Huruf");
                        textBox4.Text = textBox4.Text.Substring(0, textBox4.Text.Length - 1);
                    }
                }
                else if (comboBox1.Text == "Harga Lebih Kecil Dari")
                {
                    try
                    {
                        conn.Open();
                        string temp = "0";
                        if (textBox4.Text != "")
                        {
                            temp = textBox4.Text;
                        }
                        string query = "select * from menu where harga_menu<" + temp + "";
                        OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                        DataTable dt = new DataTable();
                        oda.Fill(dt);
                        dataGridView1.DataSource = dt;
                        conn.Close();
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("Tidak Boleh ada Huruf");
                        textBox4.Text = textBox4.Text.Substring(0, textBox4.Text.Length - 1);
                    }
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }
    }
}
