﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class checkout : Form
    {
        public checkout()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
        OracleConnection conn;
        FormUtama parent;
        private void checkout_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            try
            {
                //conn = new OracleConnection("user id=system;password=system;data source=system");
                conn = new OracleConnection("user id=system;password=system;data source=orcl");
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try {
                string query = "select t.id_transaksi,c.nama_cust from transaksi t, customer c where t.status_trans = 1 and t.id_cust = c.id_cust";
                using (OracleCommand cmd2 = new OracleCommand(query, conn))
                {
                    using (OracleDataReader dr = cmd2.ExecuteReader())
                    {
                        List<object> listtrans = new List<object>();

                        while (dr.Read())
                        {
                            listtrans.Add(new
                            {
                                Id = dr.GetString(0),
                                Nama = dr.GetString(0) + " - " + dr.GetString(1)
                            });
                        }

                        comboBox1.DataSource = listtrans;
                        comboBox1.DisplayMember = "Nama";
                        comboBox1.ValueMember = "Id";
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        int totalMakanan,totalFas,totKamar;
        string nok;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string q2 = "select id_transaksi, nama_cust, nama_pegawai, no_kamar,total_pembayaran from transaksi t, customer c, pegawai p where t.id_transaksi = '"+comboBox1.SelectedValue.ToString()+"' and t.id_cust = c.id_cust and p.id_pegawai = t.id_pegawai";
            OracleDataAdapter oda = new OracleDataAdapter(q2, conn);
            DataTable dtmn = new DataTable();
            oda.Fill(dtmn);
            if (dtmn.Rows.Count > 0)
            {
                lblNama.Text = dtmn.Rows[0].ItemArray[1].ToString();
                lblNoKamar.Text = "- "+ dtmn.Rows[0].ItemArray[3].ToString();
                lblTotKamar.Text = "Rp "+ dtmn.Rows[0].ItemArray[4].ToString();
                totKamar = Convert.ToInt32(dtmn.Rows[0].ItemArray[4].ToString());
                nok = dtmn.Rows[0].ItemArray[3].ToString();
            }

            string q3 = "select nama_menu as nama, jumlah, subtotal from pemesanan_menu p, menu m where id_transaksi = '" + comboBox1.SelectedValue.ToString() + "' and m.id_menu = p.id_menu";
            OracleDataAdapter oda1 = new OracleDataAdapter(q3, conn);
            DataTable dtmenu = new DataTable();
            oda1.Fill(dtmenu);
            dataGridView1.DataSource = dtmenu;
            totalMakanan = 0;
            for (int i = 0; i < dtmenu.Rows.Count; i++)
            {
                totalMakanan += Convert.ToInt32(dtmenu.Rows[i].ItemArray[2]);
            }
            lblTotMakanan.Text = "Rp "+ totalMakanan;

            string q4 = "select nama_fas as Nama, harga_fas as Harga, jumlah, (harga_fas*jumlah) as subtotal from usefas u, fasilitas f where id_transaksi = '" + comboBox1.SelectedValue.ToString() + "' and f.id_fas = u.id_fas";
            OracleDataAdapter oda3 = new OracleDataAdapter(q4, conn);
            DataTable dtusefas = new DataTable();
            oda3.Fill(dtusefas);
            dataGridView2.DataSource = dtusefas;
            totalFas = 0;
            for (int i = 0; i < dtusefas.Rows.Count; i++)
            {
                totalFas += Convert.ToInt32(dtusefas.Rows[i].ItemArray[3]);
            }
            lblTotFasilitas.Text = "Rp " + totalFas;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                //insert ke dtrans;
                string id = comboBox1.SelectedValue.ToString();
                string query = "insert into dtransaksi values('" + id + "','" + nok + "','" + totalMakanan + "','" + totalFas + "')";
                OracleCommand cmd = new OracleCommand(query, conn);
                cmd.ExecuteNonQuery();

                //update total bayar di transaksi
                int totalBayar = totalMakanan + totalFas + totKamar;
                string query1 = "update transaksi set total_pembayaran = "+totalBayar+"where id_transaksi = '"+id+"'";
                OracleCommand cmd1 = new OracleCommand(query1, conn);
                cmd1.ExecuteNonQuery();
                MessageBox.Show("Berhasil Checkout");

                //baru ganti status transaksi jadi sudah selesai
                string query2 = "update transaksi set status_trans = 0";
                OracleCommand cmd2 = new OracleCommand(query2, conn);
                cmd2.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }
    }
}
