﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace ProyekPCS
{
    public partial class FormTransaksi : Form
    {
        public FormTransaksi()
        {
            InitializeComponent();
        }
        OracleConnection OC;
        OracleDataAdapter oda;
        OracleCommand cmd;
        OracleCommandBuilder ocomb;
        DataTable dt;
        string query = "";
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        void Isi_Grid(string query, DataGridView DG)
        {
            oda = new OracleDataAdapter(query, OC);
            dt = new DataTable();
            ocomb = new OracleCommandBuilder(oda);
            oda.Fill(dt);
            DG.DataSource = dt;
        }
        FormUtama parent;
        private void FormTransaksi_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            label11.Text = "Halo, "+parent.getUserLogin();
            try
            {
                OC = new OracleConnection("user id=system;password=system;data source=orcl");
                OC.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Isi_Grid("SELECT NAMA_CUST AS NAMA_CUSTOMER, NOTELP_CUST AS NO_TELPON, ALAMAT_CUST AS ALAMAT FROM CUSTOMER", dataGridView2);
            //Isi_Grid("SELECT NOMOR_KAMAR,JENIS_KAMAR,DESKRIPSI_KAMAR FROM TAMPILAN_FORM_KAMAR", dataGridView1);
            // ini yang knotrol grid
            isiKamar_tersedia("");
            cmd = new OracleCommand("select AUTOGENIDTRANS from dual",OC);
            textBox1.Text = cmd.ExecuteScalar().ToString();
            timer1.Enabled = true;
            label12.Text = DateTime.Now.ToString();
        }
        void isi_Harga()
        {
            query = "select kode_jenis from kamar where no_kamar = '" + textBox2.Text + "'";
            string konis = "";
            cmd = new OracleCommand(query, OC);
            try
            {
                konis = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //unutk harga
            try
            {
                query = "select harga_kamar from jenis_kamar where kode_jenis = '" + konis + "'";
                cmd = new OracleCommand(query, OC);
                konis = cmd.ExecuteScalar().ToString();
                textBox4.Text = (Convert.ToInt32(konis) * (hari_nginap)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox2.Text = dataGridView1[0, e.RowIndex].Value.ToString();
            isi_Harga();
        }
        private void dataGridView2_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string nama = dataGridView2[0, e.RowIndex].Value.ToString();
            string no_telp = dataGridView2[1,e.RowIndex].Value.ToString();
            query = "select id_cust from customer where NAMA_CUST = '" + nama + "' and NOTELP_CUST = '" + no_telp + "'";
            cmd = new OracleCommand(query, OC);
            textBox3.Text = cmd.ExecuteScalar().ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label12.Text = DateTime.Now.ToString();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if(button1.Enabled)
            {
                isiKamar_tersedia(textBox5.Text);
            }
            else
            {
                query = "select * from transaksi where id_trans like '%" + textBox5.Text + "%' or no_kamar like '%" + textBox5.Text + "%' ";
                Isi_Grid("select * from transaksi", dataGridView1);
            }
            
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            query = "SELECT NAMA_CUST AS NAMA_CUSTOMER, NOTELP_CUST AS NO_TELPON, ALAMAT_CUST AS ALAMAT FROM CUSTOMER where NAMA_CUST like '%" + textBox6.Text + "%' " +
                "or NOTELP_CUST like '%" + textBox6.Text + "%' or ALAMAT_CUST like '%" + textBox6.Text + "%'";
            Isi_Grid(query, dataGridView2);
        }
        int hari_nginap = 0;
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            isiKamar_tersedia(textBox5.Text);
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            isiKamar_tersedia(textBox5.Text);
            //MessageBox.Show(dateTimePicker2.Value.ToString("MMM"));
        }
        void isiKamar_tersedia(string search)
        {
            DateTime dtp1 = dateTimePicker1.Value;
            DateTime dtp2 = dateTimePicker2.Value;
            TimeSpan tspan = dtp2 - dtp1;
            hari_nginap = tspan.Days + 1;
            string tgl1 = dateTimePicker1.Value.ToString("dd/MMM/yyyy");
            string tgl2 = dateTimePicker2.Value.ToString("dd/MMM/yyyy");
            string PLEASE = "SELECT * " +
                    "FROM TAMPILAN_FORM_KAMAR T " +
                    "WHERE T.NOMOR_KAMAR NOT IN( " +
                    "SELECT T.NO_KAMAR " +
                    "FROM TRANSAKSI T " +
                    "WHERE ((upper('" + tgl1 + "') >= T.TGL_CHKIN AND upper('" + tgl1 + "') <= T.TGL_CHKOUT) OR " +
                    "(upper('" + tgl2 + "') >= T.TGL_CHKIN AND upper('" + tgl2 + "') <= T.TGL_CHKOUT)) AND STATUS_TRANS = 1 " +
                    ") and (T.DESKRIPSI_KAMAR like '%"+search+ "%' or T.NOMOR_KAMAR like '%" + search + "%' or " +
                    "T.JENIS_KAMAR like '%" + search + "%') order by 1";
            try
            {
                Isi_Grid(PLEASE, dataGridView1);
                //textBox4.Text = PLEASE;
            }
            catch (Exception ex)
            {
                MessageBox.Show(PLEASE);
            }
            if(textBox2.Text.Length != 0)
            {
                isi_Harga();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(hari_nginap <=0 )
            {
                MessageBox.Show("Hari harus lebih");
            }
            else if(textBox2.Text.Length == 0 || textBox3.Text.Length == 0)
            {
                MessageBox.Show("kamar dan customer dipilih terlebih dahulu");
            }
            else
            {
                query = "insert into transaksi values('" + textBox1.Text + "',sysdate,TO_DATE('" + dateTimePicker1.Value.ToString("dd/MMM/yyyy") + "','DD/MM/YYYY')," +
                    "TO_DATE('" + dateTimePicker2.Value.ToString("dd/MMM/yyyy") + "','DD/MM/YYYY'), '" + textBox2.Text + "','"+textBox3.Text+"',"+parent.getIdUserLogin()+"," + Convert.ToInt32(textBox4.Text) + ",1)";

                try
                {
                    cmd = new OracleCommand(query, OC);
                    cmd.ExecuteNonQuery();
                    cmd = new OracleCommand("select AUTOGENIDTRANS from dual", OC);
                    textBox1.Text = cmd.ExecuteScalar().ToString();
                    MessageBox.Show("Transaksi Berhasil");
                    isiKamar_tersedia(textBox5.Text);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(button1.Enabled)
            {
                Isi_Grid("select * from transaksi", dataGridView1);
                button1.Enabled = !button1.Enabled;
                label7.Text = "Daftar Trans";
                button2.Text = "Daftar Kamar";
            }
            else
            {
                isiKamar_tersedia("");
                button1.Enabled = !button1.Enabled;
                label7.Text = "Daftar Kamar";
                button2.Text = "Daftar Trans";
            }
            

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showMenuUtama();
        }
    }
}
