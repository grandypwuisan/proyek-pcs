DROP TABLE PEGAWAI CASCADE CONSTRAINTS PURGE;
DROP TABLE FASILITAS CASCADE CONSTRAINTS PURGE;
DROP TABLE CUSTOMER CASCADE CONSTRAINTS PURGE;
DROP TABLE USEFAS CASCADE CONSTRAINTS PURGE;
DROP TABLE TRANSAKSI CASCADE CONSTRAINTS PURGE;
DROP TABLE MENU CASCADE CONSTRAINTS PURGE;
DROP TABLE PEMESANAN_MENU CASCADE CONSTRAINTS PURGE;
DROP TABLE DTRANSAKSI CASCADE CONSTRAINTS PURGE;
DROP TABLE KAMAR CASCADE CONSTRAINTS PURGE;
DROP TABLE JENIS_KAMAR CASCADE CONSTRAINTS PURGE;

CREATE TABLE PEGAWAI(
	ID_PEGAWAI VARCHAR2(5) CONSTRAINT PK_PEGAWAI PRIMARY KEY,
	NAMA_PEGAWAI VARCHAR2(50),
	NOTELP_PEGAWAI VARCHAR2(12),
	ALAMAT_PEGAWAI VARCHAR2(25),
	TGLLAHIR_PEGAWAI DATE,
	STATUS_PEGAWAI number
);

INSERT INTO PEGAWAI VALUES('CS001','KENNY LISAL','90980980','DANAUPOSO',TO_DATE('01/01/2012','DD/MM/YYYY'),1);
INSERT INTO PEGAWAI VALUES('TB001','TADA BANRI','10980980','DANAUPOSO',TO_DATE('03/01/1991','DD/MM/YYYY'),1);

CREATE TABLE FASILITAS(
	ID_FAS VARCHAR2(5) CONSTRAINT PK_FASILITAS PRIMARY KEY,
	NAMA_FAS VARCHAR2(70),
	HARGA_FAS NUMBER,
	STATUS_FAS VARCHAR2(15)
);

CREATE TABLE MENU(
	ID_MENU VARCHAR2(5) CONSTRAINT PK_MENU PRIMARY KEY,
	NAMA_MENU VARCHAR2(100),
	HARGA_MENU NUMBER
);

CREATE TABLE CUSTOMER(
	ID_CUST VARCHAR2(5) CONSTRAINT PK_CUSTOMER PRIMARY KEY,
	NAMA_CUST VARCHAR2(50),
	ALAMAT_CUST VARCHAR2(25),
	NOTELP_CUST VARCHAR2(12)
);
INSERT INTO CUSTOMER VALUES('AW001','KENNSDGFSE','90980980','DANAUPOSO');

CREATE TABLE JENIS_KAMAR(
	KODE_JENIS VARCHAR2(5) CONSTRAINT PK_JENISKAMAR PRIMARY KEY,
	NAMA_JENIS VARCHAR2(15),
	HARGA_KAMAR NUMBER,
	DESKRIPSI_KAMAR VARCHAR2(100)
);

CREATE TABLE KAMAR(
	NO_KAMAR VARCHAR2(3) CONSTRAINT PK_KAMAR PRIMARY KEY,
	KODE_JENIS VARCHAR2(5) CONSTRAINT FK_KODEJENIS REFERENCES JENIS_KAMAR(KODE_JENIS),
	STATUS NUMBER
);


CREATE TABLE TRANSAKSI(
	ID_TRANSAKSI VARCHAR2(9) CONSTRAINT PK_TRANSAKSI PRIMARY KEY,
	TGLPESAN_TRANSAKSI DATE,
	TGL_CHKIN DATE,
	TGL_CHKOUT DATE,
	NO_KAMAR VARCHAR2(3) CONSTRAINT FK_TNOKAMAR REFERENCES KAMAR(NO_KAMAR),
	ID_CUST VARCHAR2(5) CONSTRAINT FK_IDCUSTOMER REFERENCES CUSTOMER(ID_CUST),
	ID_PEGAWAI VARCHAR2(5) CONSTRAINT FK_IDPEGAWAI REFERENCES PEGAWAI(ID_PEGAWAI),
	TOTAL_PEMBAYARAN NUMBER,
	STATUS_TRANS NUMBER
);

CREATE TABLE USEFAS(
	ID_FAS VARCHAR2(5) CONSTRAINT FK_IDFAS REFERENCES FASILITAS(ID_FAS),
	ID_TRANSAKSI VARCHAR2(9) CONSTRAINT FK_TRANSAKSI REFERENCES TRANSAKSI(ID_TRANSAKSI),
	JUMLAH NUMBER,
	CONSTRAINT PK_USEFAS PRIMARY KEY (ID_FAS, ID_TRANSAKSI)
);

CREATE TABLE PEMESANAN_MENU(
	ID_TRANSAKSI VARCHAR2(9) CONSTRAINT FK_TRANSAKSI2 REFERENCES TRANSAKSI(ID_TRANSAKSI),
	ID_MENU VARCHAR2(5) CONSTRAINT FK_MENU REFERENCES MENU(ID_MENU),
	JUMLAH NUMBER,
	SUBTOTAL NUMBER,
	CONSTRAINT PK_PESAN PRIMARY KEY (ID_MENU, ID_TRANSAKSI)
);
CREATE TABLE DTRANSAKSI(
	ID_TRANSAKSI VARCHAR2(9) CONSTRAINT FK_TRANSAKSI3 REFERENCES TRANSAKSI(ID_TRANSAKSI),
	NO_KAMAR VARCHAR2(3) CONSTRAINT FK_NOKAMAR REFERENCES KAMAR(NO_KAMAR),
	BAYARAN_MENU NUMBER,
	BAYARAN_FAS NUMBER,
	CONSTRAINT PK_DTRANS2 PRIMARY KEY (NO_KAMAR, ID_TRANSAKSI)
);

CREATE OR REPLACE FUNCTION AUTOGENIDJK(TNAMA VARCHAR2)
RETURN VARCHAR2
IS 
	KODE VARCHAR2(5);
	URUTAN NUMBER;
	TSPACE NUMBER;
BEGIN 
	KODE := 'T';
	TSPACE := INSTR(TNAMA,' ', -1);
	SELECT COUNT(*) INTO URUTAN FROM JENIS_KAMAR WHERE KODE_JENIS LIKE ('%'||TNAMA||'%');
	KODE := KODE||LPAD(URUTAN + 1,2,'0');
	IF (TSPACE = 0) THEN 
		KODE := KODE || SUBSTR(TNAMA,0,2);
		
	ELSE 
		KODE := KODE || SUBSTR(TNAMA,0,1) || SUBSTR(TNAMA,TSPACE + 1,1);
	END IF;
	RETURN UPPER(KODE);
END;
/
SHOW ERR;

CREATE OR REPLACE FUNCTION AUTOGENIDTRANS
RETURN VARCHAR2
IS 
	KODE VARCHAR2(9);
	URUTAN NUMBER;
BEGIN 
	KODE := to_char(sysdate,'dd')||to_char(sysdate,'mm')||to_char(sysdate,'yy')||'T';
	SELECT COUNT(*) INTO URUTAN FROM TRANSAKSI WHERE ID_TRANSAKSI LIKE ('%'||KODE||'%');
	KODE := KODE||LPAD(URUTAN + 1,2,'0');
	RETURN KODE;
END;
/
SHOW ERR;

CREATE OR REPLACE VIEW TAMPILAN_FORM_KAMAR AS 
SELECT K.NO_KAMAR AS NOMOR_KAMAR, J.NAMA_JENIS AS JENIS_KAMAR,J.DESKRIPSI_KAMAR AS DESKRIPSI_KAMAR, (CASE 
WHEN K.STATUS = 0 THEN 'TERPAKAI'
WHEN K.STATUS = 1 THEN 'TERSEDIA'
ELSE  'PERBAIKAN'
END
)AS STATUS
FROM JENIS_KAMAR J, KAMAR K 
WHERE K.KODE_JENIS = J.KODE_JENIS
order by 1;

CREATE OR REPLACE VIEW LANTAI_FORM_KAMAR AS 
SELECT DISTINCT 'Lantai '||SUBSTR(K.NO_KAMAR,0,1) as LANTAI, SUBSTR(K.NO_KAMAR,0,1) AS KODE
FROM KAMAR K 
GROUP BY K.NO_KAMAR
UNION
select 'Semua Lantai ' as LANTAI, '' as KODE 
from DUAL
ORDER BY 1;

INSERT INTO TRANSAKSI VALUES ('123455T05', TO_DATE('11/01/1981','DD/MM/YYYY'),
TO_DATE('20/05/2019','DD/MM/YYYY'),TO_DATE('25/05/2019','DD/MM/YYYY'),'808','AW001','CS001',0);

INSERT INTO TRANSAKSI VALUES ('123455T02', TO_DATE('11/01/1981','DD/MM/YYYY'),
TO_DATE('08/01/2000','DD/MM/YYYY'),TO_DATE('10/01/2000','DD/MM/YYYY'),'201','AW001','CS001',0);

SELECT T.NO_KAMAR
FROM TRANSAKSI T
WHERE ('01-JAN-2000' >= T.TGL_CHKIN AND '01-JAN-2000' <= T.TGL_CHKOUT) OR 
('09-JAN-2000' >= T.TGL_CHKIN AND '09-JAN-2000' <= T.TGL_CHKOUT);


CREATE OR REPLACE TRIGGER INS_FASILITAS
BEFORE INSERT
ON FASILITAS
FOR EACH ROW
DECLARE
	GEN_CODEC VARCHAR2(5);
	UP_NAMA VARCHAR2(30);
	NOMOR NUMBER;
BEGIN
	UP_NAMA := :NEW.NAMA_FAS;
	GEN_CODEC:= AUTO_GEN_ID_FAS(UP_NAMA);
	:NEW.ID_FAS := GEN_CODEC;
END;
/	
SHOW ERR;


CREATE OR REPLACE FUNCTION AUTO_GEN_ID_FAS (TNAMA VARCHAR2)
RETURN VARCHAR2
IS
	IDX NUMBER;
	IDXSPASI NUMBER;
	IDXSPASIMEMBER NUMBER;
	TKODE VARCHAR2(5);
	TKODEMEMBER VARCHAR2(5);
BEGIN
	IDX :=1;
	IDXSPASI := INSTR(TNAMA,' ',-1);
	IF (IDXSPASI>0)THEN 
		TKODE := SUBSTR(TNAMA, 1,1) || SUBSTR(TNAMA, IDXSPASI+1,1);
	ELSE 
		TKODE := UPPER(SUBSTR(TNAMA, 1,2));
	END IF;
	FOR X IN (SELECT * FROM FASILITAS) LOOP
		IF((TKODE || LPAD(IDX,3,'0')) = X.ID_FAS) THEN
			IDX:=IDX+1;
		ELSE 
			IDX:=IDX+0;
		END IF;
	END LOOP;
	TKODE := TKODE || LPAD(IDX,3,'0');
	RETURN TKODE;	
END;
/
SHOW ERR;

create or replace function AUTO_GEN_ID_PEGAWAi_HOTEL(tnama varchar2)
return varchar2
is
    idx number;
    idxspasi number;
    tkode varchar2(5);
    tnm varchar(2);
    panjang number;
begin
    idxspasi := instr(tnama,' ',1);
    panjang := length(tnama);
    if(idxspasi >0 and panjang > idxspasi) then
        tnm := upper(substr(tnama,1,1))||upper(substr(tnama,idxspasi+1,1));
    else
        tnm := upper(substr(tnama,1,2));
    end if;
    select count(*) into idx from pegawai where id_pegawai like tnm||'%';
    idx := idx+1;
    tkode := tnm||lpad(idx,3,'0');
    return tkode;
end;
/

create or replace function AUTO_GEN_ID_CUST_HOTEL(tnama varchar2)
return varchar2
is
    idx number;
    idxspasi number;
    tkode varchar2(5);
    tnm varchar(2);
    panjang number;
begin
    idxspasi := instr(tnama,' ',1);
    panjang := length(tnama);
    if(idxspasi >0 and panjang > idxspasi) then
        tnm := upper(substr(tnama,1,1))||upper(substr(tnama,idxspasi+1,1));
    else
        tnm := upper(substr(tnama,1,2));
    end if;
    select count(*) into idx from customer where id_cust like tnm||'%';
    idx := idx+1;
    tkode := tnm||lpad(idx,3,'0');
    return tkode;
end;
/
